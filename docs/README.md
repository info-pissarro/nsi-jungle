# nsi_jungle

## Cékoi ?
Projet GitLab qui n'a rien d'un projet.
Depuis peu, je suis en charge d'enseigner la spécialité NSI (Numérique et Sciences Informatiques) sans être moi-même informaticien.
Tout au plus puis-je me qualifier de bidouilleur passionné.
Dans ces conditions, j'ai souvent l'impression d'être perdu dans une *jungle*.
L'image me vient ~des élèves~ de la littérature informatique abondante et la pléthore de solutions logicielles.

Pour m'y retrouver, j'utilise cet espace pour organiser les connaissances acquises au cours de cet enseignement.
Il peut s'agir de solutions techniques à mettre en oeuvre, de notions théoriques nouvelles (pour moi), des séquences pédagogiques, etc.
Bref, avant tout pour ne pas oublier, et pourquoi pas partager.

## Pourquoi un dépôt Git ?
L'idée n'est pas la mienne, j'ai simplement repris ce que j'ai vu chez certains collègues.
Cette solution possède des avantages indéniables :
- J'utilise abondamment *LaTeX* pour mes supports imprimés, mais pour ce type d'usage autant rester sur un langage à balises plus simple.
- J'aime beaucoup *html/css*, sauf que j'y passe un temps fou pour un résultat discutable.
- Pas de cloud synchronisé à mettre en oeuvre, et les erreurs qui vont avec.
- L'ENT est fonctionnel pour le partage de fichiers, mais une vraie plaie à l'usage, toi-même tu sais.
- Déléguer la maintenance du serveur, la gestion des cookies, l'observation du RGPD à ceux qui savent faire.
- Pour partager mes idées/infos en dehors du lycée.
- Comme toujours, c'est aussi l'occasion d'apprendre une nouvelle techno.

## Panorama du dépôt
- une page qui recense les différents [logiciels](./logiciels.md) utilisables par les élèves ;
- une autre avec des [outils](./outils.md) utiles à l'enseignant ;
- un [aide-mémoire](./glfm.md) sur l'usage de markdown ;
- un point sur le [hardware](./hardware.md) à faire commander ;
- des [activités](./activites) faites en classe ;
- des [didacticiels](./tutos) sur les notions que  j'ai apprises sur le tas ;
- les [vidéos](./videos.md) éclairantes sur certains points du programme.

## Todo-list personnelle

- [ ] apprivoiser pdf4teachers
- [ ] idem avec AutoMultipleChoice
