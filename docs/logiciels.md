Les contraintes pour le choix d'un programme sont multiples :
ergonomie, systèmes d'exploitation supportés, versions portables pour les ordis région,
adéquation avec le programme de NSI, etc.
Dans la mesure du possible, les programmes mentionnés dans la suite
sont multi-OS ou des applications web.


# Éditeurs/EDI

## Génériques
- Geany
- Notepad++

## Python
- Idle
- Thonny
- Spyder
- Mu

# Réseau
- WireShark
- Filius

# Portes logiques
- [logisim](http://www.cburch.com/logisim/)
- [logisim-evolution](https://github.com/logisim-evolution/logisim-evolution)
- [DLD](https://www.digitalcircuitdesign.com)
- [hneemann/Digital](https://github.com/hneemann/Digital)
- [BOOLR](http://boolr.me)
- [Digital Logic Sim](https://sebastian.itch.io/digital-logic-sim)

# Ligne de commande
- Bashcrawl
- [GameShell](https://github.com/phyver/GameShell)
- Cmder
