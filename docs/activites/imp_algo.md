# Exécution d'algorigrammes

Ce court document décrit une activité d'informatique débranchée.
Elle consiste à faire exécuter collectivement par les élèves des algorithmes.
**Lien vers page expliquant les ordis Imp de Terry Pratchett**


## Présentation sommaire de l'activité

### Cartes
- Chaque élève possède une ou plusieurs cartes indiquant les opérations élémentaires à exécuter.
- Pour savoir quel élève doit agir, les cartes sont identifiées par un numéro.
- L'instruction \emph{appeler} consiste à désigner la carte suivante qui doit être exécutée.


<div class="center">
    Image d'une carte ici
</div>


### Mise en place
1. Les cartes sont réparties entre les élèves.
2. La première carte est appelée, un feutre est remis à l'élève désigné.
3. L'élève exécute son instruction au tableau. Dans le cas d'une affectation, il s'agit de mettre à jour les valeurs des variables.
4. L'élève remet son feutre à l'élève appelé.
5. L'activité s'arrête lorsque la carte sans instruction d'appel est exécutée.
6. Pourquoi pas faire reproduire l'algorigramme aux élèves et ensuite tout implémenter en Python.


Pour ajouter une pression supplémentaire, le professeur peut chronométrer l'épreuve
et indiquer un objectif de temps.
Une fois la panique passée, comparaison des résultats, foutage de gueule, etc.
Un exercice final à envisager consiste à retrouver l'algorigramme à partir des cartes
puis l'implémenter en Python.




## Commentaires et aménagements
Après multiples tests, cette activité possède quelques écueils :
- Certains élèves peuvent rester inactifs longtemps.
- D'autres, plus impliqués, vont accaparer feutre et tableau.
- Cela dégage beaucoup de bruit en raison des déplacements et de la compétition.


### Équipes
- Les élèves sont davantage actifs lorsqu'ils sont répartis en équipe.
Chacune d'elles possède alors un feutre de couleur et un emplacement défini du tableau.
- La constitution des équipes peut se faire avec un capitaine.
Je n'ai pas d'idée à ce jour de rôle particulier supplémentaire à attribuer au capitaine.
- Les équipes contraintes &mdash; par rangées par exemple &mdash; remportent mon adhésion, surtout dans des groupes de spécialité.

### Cartes
- On peut identifier les cartes par des noms de code  plus plus ou moins fantaisistes au choix du professeur.
Pour les faire un peu plus galérer, on peut aussi mettre des figures compliquées à décrire.
Dans cette variante, je suggère de laisser les cartes face cachée pendant l'explication des règles.
- Certaines cartes reviennent bien plus souvent que d'autres.
On peut alors les symboliser par un marquage distinctif au verso, de 0 à 3 gommettes par exemple.
On répartira donc les gommettes lors de l'attribution des cartes.

### Reconstitution de l'algorigramme
- Si des aimants sont fournis, alors le tableau est un bon support.
Les feutres permettent de tracer les flèches d'appel.
À noter que les élèves n'ont pas du tout ce réflexe.
- Sinon, utiliser les tables. La représentation des flèches peut se faire avec
du matériel fourni. Brouillon et styles, ficelles, fil de fer, etc.
- Un point peu compris par les élèves : les blocs conditionnés répétés sont des boucles.

### Instructions élémentaires
On peut prendre de la distance avec la syntaxe propre aux algos ou aux langages de programmation
en poussant plus loin le côté *débranché*. Dans le cas d'un algorithme de tri, on peut par exemple :
- Représenter le tableau à trier par des cartes aimantées.
- Représenter les variables d'index par des aimants colorés à placer au dessus ou en dessous
des cartes.
- Les opérations d'échange se font alors en échangeant directement les cartes.
- Incrémenter une variable d'index revient à déplacer un élément vers la droite.
- Pourquoi pas une représentation graphique des instructions.
- Ne plus afficher des valeurs numériques mais comparer des masses colorées avec une balance Roberval.


## Outils pour le professeur

### Représentation graphique
Pour préparer cette activité, le professeur représente l'algorithme souhaité sous forme d'algorigramme. 
Une telle figure peut été obtenue avec Graphviz ou la bibliothèque python flowchart.

### Conception des cartes
Afin de rendre cette activité la plus exploitable, j'ai créé un script Python
pour générer automatiquement les cartes à imprimer.
Documentation à venir ultérieurement.
