# Tris

Cette activité se place dans ou après une séquence sur les tris.
En général, les élèves ne voient pas l'intérêt d'une démarche
algorithmique sur les exemples vus en classe.
En particulier, les tableaux de nombres
leurs apparaissent évidents du fait que les valeurs
sont toutes visibles en permanence.

## Avec des cartes aimantées
Exécutable par le professeur, cette présentation consiste
à symboliser les variables d'index avec des aimants.
Les valeurs chiffrées sont quant à elles
représentées sur des petits cartons aimantés.

### Matériel
- aimants de couleurs différentes ;
- des cartes numérotées et plastifiées ;
- bandes magnétiques découpables.

### Exécution
Le professeur exécute simplement les instructions des algorithmes exécutés.
Les aimants sont placées sous les cartes à traiter.
On peut ensuite représenter l'exécution du programme
avec un tableau de variables.

### Variantes
- Indiquer la valeur des cartes au dos pour ne les rendre
visibles qu'au cours de la comparaison, non testé.
- Ajouter une zone de transfert pour décomposer l'échange de valeurs.


## Boîtes lestées
Le but ici est de faire exécuter un algorithme
de tri pour classer par ordre de masse des boîtes lestées. Seule la balance à plateaux peut être utilisée pour comparer les masses de deux boîtes.


### Préparation

#### Matériel requis
- boîtes d'alumettes vides ;
- balance à plateaux, précise ;
- balance électronique de précision ;
- de quoi lester, petite quincaillerie par exemple ;
- de quoi matérialiser les emplacements de départ, papier par exemple ;
- éventuellement un emplacement supplémentaire pour un échange de boîtes ;
- trois objets visuellements distincts.

#### Préparation des couvercles
Les couvercles peuvent être différenciés :
cela peut se faire par un numéro, une gommette de couleur ou alors de la peinture. Le vernis est un
plus pour résister aux taches de manipulation.

Dans le cas de la peinture, le faire en deux couches pour éviter les écoulements. Suspendre les boîtes sur un fil de fer séchage facilite grandement l'application et le séchage. **photo**

Après test de l'activité, l'identification court-circuite un peu l'algorithme, les élèves se rappelant un peu certaines des comparaisons effectuées.

#### Préparation des barquettes 
Deux contraintes pour que l'activité marche :
- avoir des masses suffisamment distinctes
pour être détectables avec la balance à plateaux (15g) ;
- avoir des masses suffisamment proches pour être
difficilement comparables au toucher.

Une subtilité supplémentaire est que des couvercles peints peuvent avoir une différence de masse d'environ 20g.
Si l'on souhaite renouveler l'activité, on doit pouvoir changer les couvercles des barquettes
tout en conservant l'ordre initial.
On préparera donc des barquettes par incréments de 35g environ. Pour vérifier le résultat final des élèves,
on peut numéroter les barquettes (nombre qui restera
donc caché au cours des manipulations).

### Exécution
Annoncer clairement les contraintes aux élèves :
- trier du plus léger au plus lourd ;
- dire qu'il y a un nombre limité de pesées possibles (connu ou non selon progression) ;
- aucune note n'est prise ;
- une seule main est utilisée ;
- les boîtes ne peuvent se trouver que sur un plateau ou un emplacement initial.

### Variante collective
Les variantes contraintes supplémentaires peuvent ajouter du piment à l'activité :
- un élève effectue une seule pesée, manipule les boîtes, et appelle un suivant ;
- forcer tous les élèves à passer ;
- aucune communication pendant les manipulations ;
- établissement de la stratégie au préalable ;
- équipe vs. équipe, désignée par capitaines par exemple ;
- les autres membres ne regardent pas les manipulations précédentes.

