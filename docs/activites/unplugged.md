# Informatique débranchée

## Fournitures
N'importe quelle boutique de fournitures de bureau suffira
pour les articles les plus communs.
Après des années de boycott, Amazon se révèle être une mine
précieuse pour des achats de matériel pédagogique.
Notamment pour des quantités importantes ou des articles
inhabituels. Voici en vrac du matériel utilisé au cours
de mes activités :
- bande magnétique découpable ;
- [balance à plateaux](https://www.amazon.fr/gp/product/B08L769P66/ref=ppx_yo_dt_b_asin_title_o04_s00?ie=UTF8&psc=1) ;
- [balance de précision](https://www.amazon.fr/gp/product/B07LFC7WJH/ref=ppx_yo_dt_b_asin_title_o00_s01?ie=UTF8&psc=1) ;
- [aimants colorés](https://www.amazon.fr/gp/product/B07BTJ1K8F/ref=ppx_yo_dt_b_asin_title_o02_s00?ie=UTF8&psc=1) ;
- [perforeuse diam. 8mm](https://www.amazon.fr/gp/product/B07GVK17VD/ref=ppx_yo_dt_b_asin_title_o00_s02?ie=UTF8&psc=1) (le standard étant 6mm) ;
- [boîtes d'allumettes vides](https://www.amazon.fr/gp/product/B06Y2DZB4D/ref=ppx_yo_dt_b_asin_title_o03_s00?ie=UTF8&psc=1) ;
- [perles en bois colorées](https://www.amazon.fr/gp/product/B09PKVLQPV/ref=ppx_yo_dt_b_asin_title_o02_s01?ie=UTF8&th=1) ;
- [billes d'aluminium](https://www.amazon.fr/gp/product/B00J8JR03O/ref=ppx_yo_dt_b_asin_title_o00_s02?ie=UTF8&psc=1) ;
- [billes en bois](https://www.amazon.fr/gp/product/B089SS7ZH7/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1) ;
- pâte Fimo ;
- peinture en bombe, préférer le rayon loisirs créatifs aux grandes surfaces de bricolage (avantages très net de prix et de qualité) ;
- vernis en bombe pour recouvrir la peinture ;


## Tutos DIY

La mise en place d'activités débranchées demande de la conception mais aussi du travail manuel en amont des scéances.
Pour éviter au lecteur de réinventer l'eau chaude, ou au contraire me faire part de conseils,
j'indique ci-dessous quelques unes des manipulations effectuées.


### Peinture billes

### Billes en pâte Fimo

### Cartes aimantées

### Cartes perforées

### Peinture boîtes alumettes

### Boîtes alumettes

## Les activités

- [algorithmes de tri](tris.md) ;
- [](algorithmes de recherche) ;
- [](the game that learns) ;
- [](tour de magie binaire) ;

## Références externes
