
# BBC micro:bit

*Ce document est une synthèse des différents éléments à connaître avant d’entamer des projets sur la plateforme BBC micro:bit.
Son contenu est loin d’être exhaustif et ne saurait constituer à lui seul un cours suffisant.
Son auteur l’a avant tout pensé pour un usage personnel et s’est limité aux éléments qu’il a jugés les plus pertinents pour une exploitation en classe.
En fin de document, une séquence pédagogique est proposée pour mettre en oeuvre ces différentes notions.*

## 1 Un point sur l’écosystème micro:bit

### 1.1 Téléversement du programme
Le micro:bit se programme à l’aide d’un environnement de développement intégré (IDE) dans lequel on saisit du code haut niveau.
L’IDE compile le programme en langage machine, le résultat est un fichier au format `.hex.`
Il suffit alors de glisser-déposer ce fichier dans la mémoire du micro:bit,
reconnu comme un périphérique de stockage de masse.
Certains éditeurs permettent de faire simultanément cette étape de compilation et de
transfert. À cet effet, les éditeurs exécutés dans un navigateur requièrent la technologie WebUSB.
Seuls les navigateurs basés sur Chrome le supportent à ce jour. Sous Linux,
il y a de plus des droits à configurer <sup>[1](#x-notes-et-références)</sup> pour utiliser cette fonctionnalité.

### 1.2 MicroPython
MicroPython est une distribution du langage Python particulèrement légère, elle est
conçue pour être exécutée par des microcontrôleurs comme le micro:bit.
Lorsque le micro:bit est programmé avec MicroPython, le fichier `.hex` produit contient à la fois l’interpréteur
MicroPython et le programme réalisé. Cela permet notamment l’utilisation du micro:bit en mode interactif (ou REPL),
c’est-à-dire que l’on dialogue directement avec l’interpréteur MicroPython en cours d’exécution sur la carte.
Sous Linux, le fonctionnement du mode REPL n’est valide que si l’utilisateur *a accès à la liaison série*.

### 1.3 Les différents IDE

##### Applications
[Mu](https://codewith.mu) et [Thonny](https://thonny.org) sont deux IDE multiplateforme légers.
Ils peuvent servir d'interpréteur Python en local ou bien profiter pleinement du REPL.

##### Sites
On peut aussi passer par les plateformes ci-dessous.
Elles offrent une plus grande diversité de langages de programmation &mdash; par blocs graphiques notamment.
Certaines possèdent un simulateur intégré, permettant de voir le résultat sur un microbit virtuel.
Tous semblent supporter le téléversement direct ou bien la création d'un fichier .hex.

<table>
  <tr>
    <td></td>
    <td><a href="https://makecode.microbit.org/">MicroSoft MakeCode Editor</a></td>
    <td><a href="https://app.edublocks.org/editor">EduBlocks</a></td>
    <td><a href="https://python.microbit.org/v/3">Microbit Python Editor</a></td>
    <td><a href="https://fr.vittascience.com/microbit/?console=bottom&toolbox=vittascience">Vittascience</td>
  </tr>
  <tr>
    <td>Langages</td>
    <td>JavaScript<br>Blocs<br>Python non standard</td>
    <td>MicroPython<br>Blocs</td>
    <td>MicroPython</td>
    <td>MicroPython<br>Blocs</td>
  </tr>
  <tr>
    <td>Simulateur</td>
    <td>Oui</td>
    <td>Non</td>
    <td>Oui</td>
    <td>Oui</td>
  </tr>
</table>

## 2 Découverte du module microbit

MicroPython seul ne permet pas de manière aisée d’utiliser les fonctionnalités du micro:bit, c’est là qu’intervient le module `microbit`.

### 2.1 Importer le module
Le plus simple à l’usage est de tout importer en début de programme avec :
```python
from microbit import *
```
Sont alors importées différents sous_modules illustrés plus loin ainsi que les fonctions  de base :
* `sleep(t)` : met en pause l’exécution du programme pendant le temps t exprimé
en ms ;
* `running_time()` : retourne le temps en ms depuis le démarrage du micro:bit ;
* `panic(error_code)` : voir la doc. ;
* `reset()` : réinitialise le micro:bit.

La [documentation officielle](https://microbit-micropython.readthedocs.io/fr/latest/index.html) est composée de deux parties.
La première est intitulée &laquo; Tutorials &raquo; et contient des exemples introductifs.
La seconde se nomme &laquo; API Reference &raquo;, on y trouve toutes les classes implémentées.

### 2.2 Les LED
Les LEDs du micro:bit sont réparties selon une matrice de taille 5 × 5.
Elles sont repérées par leurs coordonnées (x ; y).

<div align="center">
  <img src="./img/exemple_led.svg" width="35%"><br>  
  La LED allumée a pour coordonnées (3 ; 2).
</div>

<br>

Le sous-module `display` propose entre autres les méthodes suvantes :
* `set_pixel(x, y, v)` : fixe la luminosité du pixel `(x, y)` à la valeur `v`, qui doit être
un entier compris entre 0 (éteint) et 9 (intense) ;
* `get_pixel(x, y)`` : retourne la luminosité du pixel `(x, y)` ;
* `clear()` : éteint toutes les LEDs.

### 2.3 Les boutons
Le module `microbit` implémente par défaut la classe [`Button`](https://microbit-micropython.readthedocs.io/fr/latest/button.html)
ainsi que les instances `button_a` et `button_b`.
Cette classe contient les méthodes :
* `is_pressed()` : retourne True si le bouton est pressé, False sinon ;
* `was_pressed()` : retourne True si le bouton a été pressé depuis le dernier appel à
cette fonction, ou depuis le début du programme s’il s’agit du premier appel ;
* `get_presses()` : retourne le nombre de pressions du bouton depuis le dernier
appel.

Dans l’exemple ci-dessous, le bouton A est appuyé au cours des intervalles en représentés en bleu.
Les numéros correspondent aux instants où les méthodes sont évaluées.

<div align="center">
  <img src="./img/boutons_axe.svg" width="30%"><br> 
</div>

|                        |   1   |   2  |   3   |   4   |   5   |
|------------------------|:-----:|:----:|:-----:|:-----:|:-----:|
| button_a.is_pressed()  | False | True |  True | False | False |
| button_a.was_pressed() | False | True | False | False |  True |
| button_a.get_presses() |   0   |   1  |   0   |   0   |   2   |


### 2.4 Les broches
Le micro:bit possède 19 broches numérotées de 0 à 20 (17 et 18 absentes)
Comme l’indique la figure ci-dessous, certaines broches sont dédiées à un rôle particulier.
Les broches sont automatiquement importées avec le module `microbit`.

<div align="center">
  <img src="./img/edge_connector_full_v1.svg" width="40%"><br> 
</div>

Pour connaître en détail le fonctionnement de chaque broche, on pourra consulter :
* [The comprehensive GPIO pinout website for the BBC micro:bit](https://microbit.pinout.xyz)
* [Edge Connector & micro:bit pinout](https://tech.microbit.org/hardware/edgeconnector/)

#### 2.4.1 Numérique
Une broche numérique peut lire ou émettre deux valeurs distinctes :
* HAUT/1 : correspond à un potentiel électrique de 3V sur le micro:bit ;
* BAS/0 : potentiel de 0V.

Les broches concernées sont P0, P1, P2, P8 et P16.

##### Écriture
Les broches du micro:bit peuvent être utilisées pour faire circuler un courant
continu dans un circuit électronique. On pourra notamment s’en servir pour éclairer des
LEDs, actionner un moteurs, etc.
Pour faire clignoter une LED alimentée par la broche P8 :
```python
from microbit import *

while True:
    pin8.write_digital(1)
    sleep(200)
    pin8.write_digital(0)
    sleep(200)
```

##### Lecture
Résistance de tirage/rappel, calibrage.

[https://nsirennes.fr/os-archi/bbc-microbit/](https://nsirennes.fr/os-archi/bbc-microbit/)

### 2.5 Liaison série

#### 2.5.1 Principe de la liaison série
Série vs. parallèle. Type bytes.

* [https://microbit-micropython.readthedocs.io/fr/latest/uart.html](https://microbit-micropython.readthedocs.io/fr/latest/uart.html)
* [https://www.seeedstudio.com/blog/2019/09/25/uart-vs-i2c-vs-spi-communication-protocols-and-uses/](https://www.seeedstudio.com/blog/2019/09/25/uart-vs-i2c-vs-spi-communication-protocols-and-uses/)

#### 2.5.2 Émission depuis le micro:bit

```python
from microbit import *

unart.init(baudrate=9600)

while True:
    if button_a.was_pressed():
        uart.write("A")
```

#### 2.5.3 Réception sur l'ordinateur

##### Gestion des droits
Retrouver script perso

##### Terminal
La commande `screen`permet d'afficher en permanence les entrées du périphérique choisi.
Après avoir branché le micro:bit, la commande suivante permet d'obtenir le chemin d'accès à la carte.

```bash
dmesh | grep tty
```

Sur tous les postes de la salle, l'identifiant était `ttyACM0`.
On peut alors lancer l'écoute en saisissant :
```bash
screen /dev/ttyACM0 9600
```

L'interface de `screen` est pour le moins austère, pour la fermer proprement, il faut alors saisir la séquence :
`ctrl + a`, `\`, `y`.
Problème de décalage résoluble avec le caractère spécial `"\r"`.

##### Python
Sous Ubuntu, on pourra installer le paquet `python3-serial` fournissant la bibliothèque `serial`.
Avec `pip` il faut en revanche installer le paquet `pyserial`.

```python
import serial

S = serial.Serial(port="/dev/ttyACM0", baudrate=9600)
print(S)

for i in range(5):
    a = S.read()
    print(a, repr(a))
```

La fonction `uart.write` permet uniquement l’envoi de type `bytes` ou `str`.


## 3 Séquence pédagogique envisageable


##### Séquence 1 - Animation diodes
* découverte du micro:bit
* documentation officielle, sous-module `display`
* révisions sur les boucles
* gestion du temps avec `sleep`
* se prête bien à l'hétérogénéité
* motifs plus ou moins compliqués selon progression : booléens, tableaux, fonctions

##### Séquence 2 - Motifs selon boutons
* classes `Image` et `Button` dans la documentation
* méthode `button.is_pressed`
* méthode `display.show`
* organisation des conditions

##### Séquence 3 - Compteur avec défilement
* méthode `button.was_pressed`
* méthode `display.scroll()`

##### Séquence 4 &mdash; Défiler l’alphabet
* synthèse
* rappel sur le caractère indexable des chaînes de caractères, ou fonction `chr`

##### Séquence 5 &mdash; Machine à opérations
* les malins penseront à faire les opérations à l’envers
* faire le lien avec le binaire
* synthèse des notions précédentes

##### Séquence 6 - Premier circuit
* refaire le diagramme logique
* rester en surface pour les explications physiques (différence de potentiel/pression d’eau)
* mettre en évidence les parties connectées de la platine
* la diode est un composant orienté
* synthèse des notions précédentes
* méthode `pin.write_digital`

##### Séquence 7 - Feu tricolore
* synthèse
* faire concevoir un diagramme
* un seul resistor grâce au sens passant des diodes

##### Séquence 8 - Compteur binaire 4 bits
* synthèse
* rappels sur la numération/information

##### Séquence 9 - Clignotement géré par resistor variable
* lecture analogique des broches
* affichage sur la sortie standard, REPL
* fonction affine de calibrage

## X. Notes et références
1. [WebUSB Troubleshooting](https://support.microbit.org/support/solutions/articles/19000105428-webusb-troubleshooting)
















