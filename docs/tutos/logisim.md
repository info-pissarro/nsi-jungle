# À la découverte des portes logiques et de Logisim-evolution

## Présentation générale
Logisim est un programme de simulation de circuits logiques et électriques.
Développé en Java, il est pouvu d'une interface graphique un peu vieillissante
mais ses fonctionnalités sont cependant remarquables :

- portes logiques usuelles ;
- circuits arithmétiques ;
- circuits mémoire ;
- création de circuits personnalisés réutilisables ;
- plus bas niveau avec les diodes et transistors ;
- analyse logique : tables de vérité, tableaux de Karnaugh.

Par rapport à certains de ses concurrents, il permet d'enregistrer ses travaux sous forme de fichier.
On peut ainsi faire partir les élèves d'une base commune mais aussi évaluer leurs productions.

Logisim n'est plus maintenu depuis 2011 mais quelques projets dérivés existent.
Le plus avancé, maintenu et documenté à ce jour est probablement [Logisim-evolution](https://github.com/logisim-evolution/logisim-evolution).
Ce dernier a une traduction française complète et une interface plus moderne.
La suite de ce guide se base sur le fonctionnement de Logisim-evolution.
La documentation complète de ce programme est incluse dans la rubrique `Aide` de son interface.



## Les composants *bas niveau*
*Logisim est pensé comme un logiciel travaillant au niveau logique.
C'est pourquoi, dans certains cas les, les composants de plus bas niveau auront des comportements inattendus.
Il convient de bien se préparer à ces situations.*

Les composants présentés dans cette section se trouvent dans la librairie `Wiring/Branchements`, accessible depuis le panneau latéral.

### Broches
- Une broche est une entrée ou une sortie d'un circuit logique, cette fonction est spécifiée par l'attribut `sortie`.
- Sous Logisim-evolution, son apparence (modifiable dans les attributs) par défaut est celle d'une flèche indiquant le sens de circulation de l'information.
- L'attribut `trois états`[^trois_etats] permet aux broches d'entrée de véhiculer un signal *flottant* (représenté par un U et câble bleu),
il n'a pas d'effet sur les broches de sortie.
- L'attribut `largeur de données` sert à spécifier le nombre de bits transmis/reçus.
- L'attribut `comportement` permet de changer la valeur d'une entrée lorsqu'aucun signal ne lui est communiqué.

### Sonde
Une sonde permet d'afficher l'état logique véhiculé dans un branchement.
Il n'y a rien à configurer, juste le relier. En particulier il n'a pas de rôle au sens d'un circuit logique.
On peut lu'utiliser en tant qu'outil de débogage.

### Buffers

### Buffers contrôlés

### Transistors
Dans l'industrie, il existe une kyrielle de transistors différents, ayant chacun leur intérêt.
Pour simplifier les choses, je reprendrai la terminologie des [transistors à effet de champ](https://fr.wikipedia.org/wiki/Transistor_%C3%A0_effet_de_champ).
Le passage du courant électrique entre la *source* (S) et le *drain* (D) est contrôlé par un champ électrique au niveau de la *grille* (G/gate).

- Les transistors de type N transmettent un 1 logique (ou *strong 1*) lorsque la tension entre la grille et la source est positive (V<sub>GS</sub>&gt;0).
- Les transistors de type P transmettent un 0 logique (ou *strong 1*) lorsque la tension entre la grille et la source est positive (V<sub>GS</sub>&lt;0).

Les développeurs de Logisim-evolution ont changé le comportement des transistors par rapport à Logisim.
Cela reste cependant un modèle simplifié par rapport à la réalité physique.
Les transistors représentés ont l'apparence des NMOS et PMOS, la documentation interne donne les tables de vérité suivantes :

<div align="center">
    <img src="./img/logisim_transistors_ink.svg" width="400px" >
</div>

## Une histoire de familles
Les propriétés physiques des transistors peuvent être utilisées pour implémenter des portes logiques.
Il y a cependant différentes manières de les utiliser pour réaliser les mêmes opérations logiques,
c'est approches sont classées selon des [familles](https://en.wikipedia.org/wiki/Logic_family).
En voici certaines que l'on peut expérimenter sous Logisim-evolution :

### Logique PMOS/NMOS
- Les signaux à traiter sont mis aux grilles des transistors tandis que drain et source sont reliées à une alimentation de niveau constant (VDD/VSS/GND).
- Une résistance de tirage (pull-up) est appliquée au drain pour forcer la sortie à 0 lorsque le transistor est bloquant.

<div align="center">
    <img src="./img/pmos_inverter.svg" height="300px" >
    Inverseur PMOS
</div>


### Logique NMOS

### Lgique CMOS

### Logique PTL

## L'opérateur not

<div align="center">
    <img src="./img/logisim_non_incomplet.svg" width="400px" >
</div>

#### RTL (resistor, transistor, L?)

<div align="center">
    <img src="./img/logisim_non_rappel.svg" width="400px" >
</div>



Un exercice intéressant serait par exemple de faire programmer la porte logique `non`.



Une solution envisageable est de mettre en place un *pull-down network*, constitué de résistances de tirage ou de rappel.



Une autre possibilité est d'assembler un circuit de type CMOS, utilisant des transistors de type N et P.
<div align="center">
    <img src="./img/logisim_non_cmos.svg" width="420px" >
</div>



## Documentation


### Pour aller plus loin
- [http://www.courstechinfo.be/Hard/SemiConducteur.html](http://www.courstechinfo.be/Hard/SemiConducteur.html)
- [https://www.allaboutcircuits.com/technical-articles/introduction-to-pass-transistor-logic/](https://www.allaboutcircuits.com/technical-articles/introduction-to-pass-transistor-logic/)
- [https://wiki.analog.com/university/courses/electronics/electronics-lab-28](https://wiki.analog.com/university/courses/electronics/electronics-lab-28)
- [https://www.youtube.com/watch?v=Jw_HIktWMQg](https://www.youtube.com/watch?v=Jw_HIktWMQg)
- [https://github.com/logisim-evolution/logisim-evolution/issues/1450](https://github.com/logisim-evolution/logisim-evolution/issues/1450)
- [https://github.com/logisim-evolution/logisim-evolution/issues/504#issuecomment-779756305](https://github.com/logisim-evolution/logisim-evolution/issues/504#issuecomment-779756305)
- [https://www.tutorialspoint.com/vlsi_design/vlsi_design_mos_inverter.htm](https://www.tutorialspoint.com/vlsi_design/vlsi_design_mos_inverter.htm)
- [http://electronique71.com/theories-mosfet-canal-n-et-canal-p/](http://electronique71.com/theories-mosfet-canal-n-et-canal-p/)

### Notes de bas de page
[^trois_etats]: https://fr.wikipedia.org/wiki/Sortie_%C3%A0_trois_%C3%A9tats

