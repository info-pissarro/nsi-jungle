# Créer un serveur HTTP sur un RaspberryPi

Les raisons de créer un serveur sur un RPi sont multiples d'un point de vue pédagogique :
- présentation d'un SoC ;
- implémentation matérielle et logicielle d'un réseau ;
- administration en ligne de commande ;
- challenges de type CTF ;
- appels à une base de données.

## Installation de l'OS

Ubuntu Server

## Les solutions logicielles

### PHP

### Python

## Modules tiers

### Base de données MariaDB