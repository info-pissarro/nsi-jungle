# La boîte à outils du prof

## Activités en ligne

## Faire des jolis supports

Reprendre ici le pdf entamé

## Maintenir sa salle avec FOG
[FOG Project](https://fogproject.org) est utilisé pour la sauvegarde ou le clonage d'images disque à travers un réseau local.
Il peut aussi servir à déployer des logiciels sur des clients déjà fonctionnels.
Windows et Linux sont tous les deux supportés.
Les pages d'explication trouvées sont nombreuses et se contredisent parfois, y compris sur le wiki officiel.
La feuille de route qui suit ne vise pas à être exhaustive et sert juste d'aide-mémoire face aux soucis rencontrés

### Mise en place
Un serveur debian ou Ubuntu fera parfaitement l'affaire, ici c'est Ubuntu serveur 20.04 qui a été choisi. La 22.04 n'est pour le moment pas supportée par FOG.

#### Configuration du serveur FOG
1. Installer le serveur, mise à jour des paquets, ne pas oublier le service `ssh`. Installer aussi la commande `unzip`.
2. Connexion via `ssh` puis passage en root avec `sudo -i`.
3. Configurer le proxy en ajoutant les lignes suivantes à la fin de `/etc/environment`.
```
http_proxy="http://10.0.0.1:3128/"
https_proxy="http://10.0.0.1:3128/"
ftp_proxy="http://10.0.0.1:3128/"
no_proxy="localhost,10.0.0.1,::1,172.20.*"
```
4. Tester avec la commande :
```bash
curl --silent -kOL https://fogproject.org/inits/init.xy
```
En cas d'échec lié au proxy, créer le fichier `/root/.curlrc` et y inscrire `proxy = http://10.0.0.1:3128`.

5. Téléchargement du projet, toujours en root.
```bash
cd /root
git clone https://github.com/fogproject/fogproject.git fogproject-dev-branch
```
Au besoin, configurer le proxy pour `git` :
```
git config --global http.proxy http://10.0.0.1:3128
```
6. Lancer le script d'installation.
```
cd fogproject-dev-branch/bin
./installfog.sh
```
7. Non partout sauf à la dernière puis attendre.
8. Connexion à l'url indiquée pendant l'installation, création/mise à jour de la base de données.
9. Se connecter à l'interface d'administration :
`http://172.20.13.54/fog`, le compte administrateur est `fog` et son mot de passe associé est `password`.

#### Côté se4 (dhcp)
SambaÉdu assure beaucoup de services y compris dhcp. Il faut faire en sorte que les futurs hôtes trouvent directement le serveur FOG.
1. Se rendre sur la page d'administration du se4 : `http://172.20.100.14/admin`.
2. Dans `dhcp/connexions actives`, réserver une IP pour le fog, qui devrait apparaître connecté. Ici l'IP retenue est `172.20.13.54`.
3. Sur la page de configuration du dhcp, rajouter la valeur `/etc/sambaedu/fog.inc` pour la clé `extra-option`.
4. Sur le se4 en root, créer le fichier  `/etc/sambaedu/fog.inc` et y inscrire les lignes :
```
next-server 172.20.13.54;
filename "undionly.kpxe";
```
5. Redémarrer le service dhcp du se4 :
```bash
# service isc-dhcp-server restart
```
6. Vérifier que le fichier .inc figure bien dans `/etc/dhcp/dhcpd.conf`.

### Utilisation

#### Côté serveur
- Créer une image, en spécifiant l'OS.
- Créer des groupes, correspondant aux salles ou aux images par exemple.
- Pour chaque groupe, associer les régles voulues : image, snapins, domaine...

#### Côté clients (hôtes)
- Dans le bios, activer le Wake-on-LAN et s'assurer qu'un boot par WOL bascule automatiquement sur PXE.
- Saisir le compte `fog:password`. Attention : qwerty par défaut.
- Booter sur PXE, puis lancer la `Full Registration`.
- Lorsque des options sont demandées, afficher le listing avec `?`, puis saisir le numéro indiqué à gauche et non la valeur.

#### Déploiement d'image
- Éteindre tous les clients de la salle.
- Préparer un poste modèle prêt à être exploité et intégré au domaine.
- *client installé en amont* ?
- Sur l'interface Web de FOG, demander la capture du poste modèle.
Celui-ci devrait alors booter de lui-même et une barre de progression apparaître des deux côtés.
- Sélectionner les postes de la salle et lancer le déploiement.
- Enjoy.


### Reste
- régler le temps d'affichage de l'interface PXE
- régler la langue du serveur
- régler la langue du clavier dans l'interface PXE, qwerty par défaut

### Documentation

- [https://wiki.fogproject.org/wiki/index.php?title=Main_Page](Wiki officiel)


## Todo-list personnelle

- [ ] apprivoiser pdf4teachers
- [ ] idem avec AutoMultipleChoice
- [ ] mermaid
